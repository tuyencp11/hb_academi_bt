﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBackGroud : MonoBehaviour
{
    [SerializeField] GameObject objTileMove;
    [SerializeField] GameObject objTileStop;
    public int id;
    [SerializeField] GameObject objBg;
    public void SpawmTileMove(Transform tfspawm)
    {
        
        GameObject newtile= Instantiate(objTileMove, transform.position, Quaternion.identity,this.transform);
        newtile.transform.SetParent(tfspawm);
        Destroy(this.gameObject);
    }
    public void SpawmTileStop(Transform tfspawm)
    {

        GameObject newtile = Instantiate(objTileStop, transform.position, Quaternion.identity, this.transform);
        newtile.transform.SetParent(tfspawm);
        Destroy(this.gameObject);
    }
}
