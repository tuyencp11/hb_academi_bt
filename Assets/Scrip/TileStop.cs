﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class TileStop : TileStopController
{
    
}
[System.Flags]
public enum MOVE_ABILITY
{
    MOVE_LEFT = 1 << 2,
    MOVE_RIGHT = 1 << 3,
    MOVE_UP = 1 << 4,
    MOVE_DOWN = 1 << 5,
    All = MOVE_LEFT | MOVE_RIGHT | MOVE_UP | MOVE_DOWN,
}
