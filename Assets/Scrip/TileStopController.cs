using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileStopController : MonoBehaviour
{

    public MOVE_ABILITY MoveAbility;
    [SerializeField] GameObject objTileOff;

    public bool checkCollider;
    public bool checkSpawm;

    private void Start()
    {
        checkSpawm = true;
    }
    private void OnTriggerEnter(Collider other)
    {

        checkCollider = true;
        objTileOff.SetActive(false);

    }

    private void OnTriggerExit(Collider other)
    {
        checkCollider = false;
        checkSpawm = false;

    }
    public bool CanMoveWithDirect(MOVE_ABILITY t)
    {
        if (MoveAbility == (MoveAbility | t))
            return true;
        else
            return false;
    }
}
