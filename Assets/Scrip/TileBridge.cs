using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBridge : MonoBehaviour
{
    [SerializeField] GameObject _bride;

    public float _maxBride;

    private bool isTrigger;

    public float time;

    public float Speed;

    private IEnumerator ScaleBride()
    {
        while (!isTrigger)
        {
            yield return new WaitForSeconds(time);
            _bride.transform.localScale += new Vector3(0, 0,Speed*time);
            
        }
    }

    public void OnScaleBride()
    {
        _bride.SetActive(true);
        StartCoroutine(ScaleBride());
    }

    private void OnTriggerEnter(Collider other)
    {

    }

    private void OnTriggerExit(Collider other)
    {
        isTrigger = true;
        StopAllCoroutines();
    }
}
