﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class CameraFollowPlayer : MonoBehaviour
{
    public Transform player; // Tham chiếu tới Transform của player
    public Vector3 offset; // Khoảng cách giữa camera và player

    void LateUpdate()
    {
        // Kiểm tra nếu player không null
        if (player != null)
        {
            // Cập nhật vị trí của camera
            transform.position = player.position + offset;
        }
    }
}
