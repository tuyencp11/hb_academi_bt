﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float Speed;
    [SerializeField] int currentTile;
    private Vector2 startTouchPosition;
    private Vector2 endTouchPosition;
    Vector3 directionPlayer;
    [SerializeField] bool canMoveStraight = true; // Biến để xác định xem có thể đi thẳng không
    [SerializeField] bool canMoveLeft = false;    // Biến để xác định xem có thể rẽ trái không
    [SerializeField] bool canMoveRight = false;   // Biến để xác định xem có thể rẽ phải không
    [SerializeField] bool canMoveBack = false;    // Biến để xác định xem có thể đi lùi không
    [SerializeField] GameObject objTile;
    [SerializeField] List<GameObject> lsTile;
    [SerializeField] Transform tfStart;
    
    private void Start()
    {
        timeDespawm = 1 / Speed;
        Speed = 5;
        currentTile = 0;
        directionPlayer = Vector3.zero;
        this.transform.position = new Vector3(tfStart.localPosition.x, 3, tfStart.localPosition.z);
    }

    void Update()
    {
        HandleSwipe();
        
        if (CanMoveInDirection(directionPlayer))
        {
            Move(directionPlayer);
        }
    }

    public bool CanMoveInDirection(Vector3 direction)
    {
        if (direction == Vector3.forward && canMoveStraight) return true;
        if (direction == Vector3.left && canMoveLeft) return true;
        if (direction == Vector3.right && canMoveRight) return true;
        if (direction == Vector3.back && canMoveBack) return true;
        return false;
    }

    private void ResetDirectionMove(TileStop temp)
    {
        canMoveStraight = false;
        canMoveBack = false;
        canMoveLeft = false;
        canMoveRight = false;
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_UP))
        {
            canMoveStraight = true;
        }
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_DOWN))
        {
            canMoveBack = true;
        }
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_LEFT))
        {
            canMoveLeft = true;
        }
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_RIGHT))
        {
            canMoveRight = true;
        }
    }
    private void NavigationMove(TileNavigation temp)
    {
        canMoveStraight = false;
        canMoveBack = false;
        canMoveLeft = false;
        canMoveRight = false;
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_UP))
        {
            canMoveStraight = true;
            canMoveBack = true;
            directionPlayer = Vector3.forward;
        }
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_DOWN))
        {
            canMoveBack = true;
            canMoveStraight = true;
            directionPlayer = Vector3.back;
        }
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_LEFT))
        {
            canMoveLeft = true;
            canMoveRight = true;
            directionPlayer = Vector3.left;
        }
        if (temp.CanMoveWithDirect(MOVE_ABILITY.MOVE_RIGHT))
        {
            canMoveLeft = true;
            canMoveRight = true;
            directionPlayer = Vector3.right;
        }
    }

    void HandleSwipe()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                startTouchPosition = touch.position;
            }

            if (touch.phase == TouchPhase.Ended)
            {
                endTouchPosition = touch.position;

                Vector2 direction = endTouchPosition - startTouchPosition;
                direction.Normalize();
                if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                {
                    if (direction.x > 0)
                    {
                        // Vuốt sang phải
                        directionPlayer = Vector3.right;
                    }
                    else
                    {
                        // Vuốt sang trái
                        directionPlayer = Vector3.left;
                    }
                }
                else
                {
                    if (direction.y > 0)
                    {
                        // Vuốt lên
                        directionPlayer = Vector3.forward;
                    }
                    else
                    {
                        // Vuốt xuống
                        directionPlayer = Vector3.back;
                    }
                }
            }
        }
    }

    void Move(Vector3 direction)
    {
        Vector3 targetPosition = transform.position + direction;
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, Speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Stop"))
        {
            var temp = other.gameObject.GetComponent<TileStop>();
            if(temp.checkSpawm)
            {
                SpamTile();
            }
            if (!temp.checkCollider)
            {
                Debug.Log("????????");
                directionPlayer =Vector3.zero;
                ResetDirectionMove(temp);  
            }
        }
        if (other.gameObject.CompareTag("Food"))
        {
            SpamTile();
        }
        if(other.gameObject.CompareTag("Bridge"))
        {
            StartCoroutine(CoutTimeDespawmTile());
            var temp = other.gameObject.GetComponent<TileBridge>();
            temp.time = timeDespawm;
            temp.Speed = Speed;
            temp.OnScaleBride();
        }
        if(other.gameObject.tag == "navigation")
        {
            var temp = other.gameObject.GetComponent<TileNavigation>();
            if (temp.checkSpawm)
            {
                SpamTile();
            }
            if (!temp.checkCollider)
            {
                directionPlayer = Vector3.zero;
                NavigationMove(temp);
            }
        }    
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Bridge"))
        {
            StopAllCoroutines();
        }
    }

    float timeDespawm;

    IEnumerator CoutTimeDespawmTile()
    {
        yield return new WaitForSeconds(timeDespawm);
        DeleteTile();
        StartCoroutine(CoutTimeDespawmTile());
    }
    float height;

    [Button]
    private void SpamTile()
    {
        this.transform.position += new Vector3(0, 0.2f, 0);
        Vector3 tfSpawm;
        height = 0.2f * (lsTile.Count);
        tfSpawm = new Vector3(0, -height, 0);
        GameObject temp = SimplePool2.Spawn(objTile, tfSpawm, Quaternion.Euler(-90, 0, 0));
        temp.transform.SetParent(this.transform);
        temp.transform.localPosition = tfSpawm;
        lsTile.Add(temp);
    }
    [Button]
    private void DeleteTile()
    {
        if (lsTile.Count > 0)
        {
            this.transform.position -= new Vector3(0, 0.2f, 0);
            SimplePool2.Despawn(lsTile[lsTile.Count - 1]);
            lsTile.RemoveAt(lsTile.Count - 1);
        }
    }
}
