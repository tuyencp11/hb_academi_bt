﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class MatrixCreateMap : MonoBehaviour
{
    public GameObject tilePrefab; // Prefab của ô vuông
    public  int gridWidth = 7; // Chiều rộng của ma trận
    public  int gridHeight = 7; // Chiều cao của ma trận
    public float tileSpacing = 1f; // Khoảng cách giữa các ô
    public Transform tfSpawmMap;
    int cout = 0;
    [SerializeField]List<TileBackGroud> tile;
    [SerializeField]List<Obstacel> lsObstacels;
    [SerializeField] GameObject objBridge;
    [SerializeField] int coutNumberobjBridge;

    private void Awake()
    {
        cout = 0;
        //spawmObstacle();
    }


    [Button]
    void GenerateGrid()
    {
        for (int x = 0; x < gridWidth; x++)
        {
            for (int z = 0; z < gridHeight; z++)
            {
                // Tính toán vị trí của mỗi ô
                Vector3 position = new Vector3(x * tileSpacing,0.55f , z * tileSpacing);

                // Tạo ô tại vị trí đã tính toán
                TileBackGroud spawm = Instantiate(tilePrefab, position, Quaternion.identity).GetComponent<TileBackGroud>();
                spawm.transform.SetParent(this.transform);
                spawm.gameObject.name = "" + cout;
                spawm.GetComponent<TileBackGroud>().id = cout;
                cout++;
                CreateListObstacle();
                tile.Add(spawm);
            }
        }
        
    }
    void CreateListObstacle()
    {
        Obstacel numberobstacles = new Obstacel();
        numberobstacles.id = cout;
        numberobstacles.idObstacel = 0;
        lsObstacels.Add(numberobstacles);
    }
    [Button]
    void spawmObstacle()
    {
        for(int i = 0; i< lsObstacels.Count; i++)
        {
            if(lsObstacels[i].idObstacel==1)
            {
                tile[i].SpawmTileMove(this.transform);
            }
            if(lsObstacels[i].idObstacel == 2)
            {
                tile[i].SpawmTileStop(this.transform);
            }
        }
    }
    [Button]
    void Createbridge()
    {
        
        Instantiate(objBridge, Vector3.zero, Quaternion.identity, transform);
    }
    [Button]
    void ResetAll()
    {
        cout = 0;
        lsObstacels.Clear();
        tile.Clear();
        foreach (var temp in tile)
            Destroy(temp.gameObject);
        
    }
}
[Serializable]
public class Obstacel
{
    public int id;
    public int idObstacel;



}

